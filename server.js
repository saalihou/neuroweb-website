// Required middlewares
var express = require('express');

// Global configuration
var env = process.env.NODE_ENV || 'development';
var config = require('./config/global.js')[env];
var globals = {};

var app = express();

// redirect http request to https protocol url
app.get('*', function(req, res, next) {
	if (env === 'production' && req.headers['x-forwarded-proto'] === 'http') {
		res.redirect(config.baseurl + req.url)
	} else {
		next();
	}
});

// Modules configuration
require(config.configs.express)(app, config, globals); // express
require(config.configs.passport)(app, config, globals); // passport
require(config.configs.routes)(app, config, globals); // routes

app.listen(config.port);