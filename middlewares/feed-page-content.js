module.exports = function (app, config, globals) {
	var middleware;
	var db = require(config.helpers.dbgetters)(app, config, globals);
	var Content = db.getContentsDb();

	middleware = function (req, res, next) {
		// temporary
		req.language = 'fr';
		Content.findOne({'page': req.path}, function(err, content) {
			if (err) {
				// TODO Custom error age
				res.sendStatus(500);
			}

			res.params = res.params || {};
			if (content && content[req.language]) {
				res.params.content = content[req.language].contentsObj;	
			} else {
				res.params.content = {};
			}
			next();
		});
	}

	return middleware;
}