module.exports = function (app, config, globals) {
	var middleware;

	middleware = function (req, res, next) {
		if (req.user) {
			next();
		} else {
			// TODO: redirect to custom error page
			res.sendStatus(401);
		}
	}

	return middleware;
}