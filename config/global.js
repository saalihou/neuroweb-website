// Required middlewares
var path = require('path');

// Global configuration options for the server
var config = {
	development: {
	},

	production: {
	}
};

var dev = {}; // Contains development options
// Generic options
dev = {
	env: 'development',
	port: process.env.PORT || 5000,
	baseurl: 'http://localhost:' + 5000,
	rootPath: path.normalize(__dirname + '/..')
};

// Other configuration files locations
var configDirectory = __dirname;
dev.configs = {
	express: path.join(configDirectory, 'express.js'),
	routes: path.join(configDirectory, 'routes.js'),
	passport: path.join(configDirectory, 'passport.js'),
};

// Middlewares
var middlewaresDirectory = path.join(dev.rootPath, 'middlewares');
dev.middlewares = {
	checklogin: path.join(middlewaresDirectory, 'checklogin'),
	feedpagecontent: path.join(middlewaresDirectory, 'feed-page-content'),
}

// Helpers
var helpersDirectory = path.join(dev.rootPath, 'helpers');
dev.helpers = {
	dbgetters: path.join(helpersDirectory, 'dbgetters'),
}

// Databases
var dbsDirectory = path.join(dev.rootPath, 'dbs');
dev.dbs = {
	users: path.join(dbsDirectory, 'users.db'),
	contents: path.join(dbsDirectory, 'contents.db'),
	prestations: path.join(dbsDirectory, 'prestations.db'),
}


var prod; // Contains production options
prod = JSON.parse(JSON.stringify(dev)); // we'll first copy the development options, then we will overwrite some parts

prod.baseurl = 'https://neuroweb.herokuapp.com';
prod.env = 'production';

config.development = dev;
config.production = prod;

module.exports = config;