module.exports = function (app, config, globals) {
	// Required modules
	var passport = require('passport'),
		fs = require('fs'),
		path = require('path'),
		db = require(config.helpers.dbgetters)(app, config, globals);

	var User = db.getUsersDb();

	var loginStrategy,
		subscribeStrategy;

	var strategiesDir = path.join(config.rootPath, 'passport-strategies');

	loginStrategy = require(path.join(strategiesDir, 'login'))(app, config, globals);

	passport.serializeUser(function (user, done) {
		console.log('serializing', user);
		return done(null, user._id);
	});

	passport.deserializeUser(function (id, done) {
		console.log('deserializing', id);
		User.findOne({_id: id}, function (err, user) {
			console.log('deserialized', user);
			if (err) {
				return done(err, false);
			}

			return done(null, user);
		})
	});

	passport.use('login', loginStrategy);

	app.use(passport.initialize());
	app.use(passport.session());

	return passport;
}