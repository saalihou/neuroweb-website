module.exports = function (app, config, globals) {

	// Required modules
	var fs = require('fs');
	var path = require('path');

	var routes = [];

	routes = fs.readdirSync(path.join(config.rootPath, 'routes'));

	routes.forEach(function(routeFile) {
		require(path.join(config.rootPath, 'routes', routeFile))(app, config, globals);
	});
	
};