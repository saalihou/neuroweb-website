// Express configuration options
module.exports = function (app, config, globals) {

	var express = require('express'),
		path = require('path'),
		stylus = require('stylus'),
		logger = require('morgan'),
		bodyParser = require('body-parser'),
		session = require('cookie-session'),
		compression = require('compression'),
		favicon = require('serve-favicon');

	// compression
	app.use(compression({
		threshold: '10kB'
	}));

	// jade
	app.set('views', path.join(config.rootPath, 'views'));
	app.set('view engine', 'jade');

	// morgan
	app.use(logger('dev'));

	// bodyparser
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({ extended: false }));

	// stylus
	app.use(stylus.middleware({
		src: path.join(config.rootPath, 'public' ,'stylus'),
		dest: path.join(config.rootPath, 'public'),
	}));

	// static
	app.use(express.static(path.join(config.rootPath, 'public')));

	// favicon
	app.use(favicon(__dirname + '/../public/images/favicon.ico'));

	// session
	app.use(session({
		name: 'neuroweb',
		keys: ['/e1p[q1/~21d'],
	}));

	// baseurl & env
	app.get('*', function(req, res, next) {
		res.params = res.params || {};
		res.params.baseurl = config.baseurl;
		res.params.env = config.env;
		next();
	});

};