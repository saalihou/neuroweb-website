module.exports = function (app, config, globals) {

	// Required modules
	var LocalStrategy = require('passport-local').Strategy,
		db = require(config.helpers.dbgetters)(app, config, globals);

	var User = db.getUsersDb();

	var strategy = new LocalStrategy({
		passReqToCallback: true,
	}, function (req, username, password, done) {

		User.findOne({username: username, password: password}, function (err, user) {
			if (err) {
				return done(err, false);
			}

			if (!user) {
				return done(null, false);
			}

			return done(null, user);
		});

	});

	return strategy;
}