module.exports = function (app, config, globals) {

	var dbGetters = {};
	var DataStore = require('nedb');

	var getDb = function(dbname) {
		if (!globals[dbname + 'db']) {
			globals[dbname + 'db'] = new DataStore({
				filename: config.dbs[dbname],
				autoload: true,
			});
		}

		return globals[dbname + 'db'];
	}

	dbGetters.getUsersDb = function () {
		return getDb('users');
	}

	dbGetters.getContentsDb = function () {
		return getDb('contents');
	}

	dbGetters.getPrestationsDb = function() {
		return getDb('prestations');
	}

	return dbGetters;

}