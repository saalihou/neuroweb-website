app.controller('PortfolioController', ['$scope', '$interval',
	function($scope, $interval) {
		$scope.firstSquareColorMap = [
			'#3AB6F2',
			undefined,
			undefined,
			undefined,
			'#3AC2C2',
			'#38BA92',
			undefined,
			undefined,
			'#00B64F',
			'#37DA7E',
			'#B2F88C',
			undefined,
			undefined,
			'#B9F73E',
			'#FFFF73',
			'#F8F8F8'
		];
		

		$scope.secondSquareColorMap = [
			'#FFFFC5',
			'#FFF2C5',
			undefined,
			'#FFF88F',
			'#FFD873',
			'#FF8900',
			'#F8F8F8',
			'#FFB440',
			undefined,
			undefined,
			'#FF7400',
			'#FF5900'
		];

		if (angular.element(window).width() > 767) {
			angular.element('.wrappers-wrapper').css('margin-top', '-40%');	
		}

		var swapColorsRegularly = function(colorMap, interval) {
			$interval(function() {
				var randomIndex1,
					randomIndex2,
					tmp;

				do {
					randomIndex1 = _.random(0, colorMap.length - 1);
				} while (typeof colorMap[randomIndex1] === 'undefined');

				do {
					randomIndex2 = _.random(0, colorMap.length - 1);
				} while (typeof colorMap[randomIndex2] === 'undefined');

				tmp = colorMap[randomIndex1];
				colorMap[randomIndex1] = colorMap[randomIndex2];
				colorMap[randomIndex2] = tmp;
			}, interval)
		};

		swapColorsRegularly($scope.firstSquareColorMap, 1000);
		swapColorsRegularly($scope.secondSquareColorMap, 1000);
	}
]);