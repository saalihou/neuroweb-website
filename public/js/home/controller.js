app.controller('HomeController', ['$scope', '$interval', '$timeout',
	function($scope, $interval, $timeout) {
		var numOfQualities = 2,
			animationDuration = 600,
			animationInterval = 3000,
			activeQualityBackup; // since we're setting the value of the active quality to null, for animation purposes, we need something to retain its value
		$scope.activeQuality = 0;

		angular.element('.discover-wrapper').show(); // show the discover part when we're done initializing stuff

		$interval(function() {
			activeQualityBackup = $scope.activeQuality;
			$scope.activeQuality = null;
			$timeout(function() {
				$scope.activeQuality = activeQualityBackup + 1;
				if ($scope.activeQuality === numOfQualities) {
					$scope.activeQuality = 0;
				}
			}, animationDuration);
		}, animationInterval);
	}
]);