$(document).ready(function() {

	var images = $('.prestations-images'),
		infos = $('.prestations-infos');

	// slick configuration
	images.slick({
		inifinite: true,
		// autoplay: true,
		asNavFor: '.prestations-infos',
		prevArrow: '.navigation-item.left',
		nextArrow: '.navigation-item.right',
		responsive: [
			{
				breakpoint: 767,
				settings: "unslick"
			}
		]
	});

	infos.slick({
		arrows: false,
		inifinite: true,
		// autoplay: true,
		asNavFor: '.prestations-images',
		responsive: [
			{
				breakpoint: 767,
				settings: "unslick"
			}
		]
	});

	var	imageWrapper = $('.prestation-image-wrapper'),
		infoWrapper = $('.prestation-info-wrapper'),
		indicatorWrapper = $('.prestations-item-index-indicator-wrapper');
	
	// display previously css-hidden elements when the slide is ready to kick in
	imageWrapper.show();
	infoWrapper.show();
	$('.navigation-wrapper').show();

	imageWrapper.children('.prestation-image').css('top', '50%').css('transform', 'translateY(-50%)');
	// height adjustment
	var docHeight = $(document).outerHeight();
	var headerHeight = $('.header-page').outerHeight();
	var footerHeight = $('.page-footer').outerHeight();
	var innerContentHeight = docHeight - headerHeight - footerHeight;
	imageWrapper.outerHeight(innerContentHeight);

	images.on('beforeChange', function() {
		imageWrapper.outerHeight(innerContentHeight);
	});

	// indicator
	var numOfItems = $('.prestation-image-wrapper').not('.slick-cloned').length;
	var indicatorHeight = innerContentHeight / numOfItems;
	indicatorWrapper.outerHeight(indicatorHeight);

	images.on('afterChange', function(event, slick, currentSlide, nextSlide) {
		indicatorWrapper.animate({
			top: indicatorHeight * slick.currentSlide,
		})
	});

	// set focus on page load
	$('.prestation-image-wrapper:first-child').focus();

	// reset focus on slide
	var resetFocus;
	infos.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
		if (infoWrapper.is(':focus')) {
			resetFocus = true;
		} else {
			resetFocus = false;
		}
	});

	infos.on('afterChange', function(event, slick, currentSlide, nextSlide) {
		if (resetFocus) {
			window.setTimeout(function() {
				$(slick.$slides[currentSlide]).focus();
			}, 50);
		}
	});

});