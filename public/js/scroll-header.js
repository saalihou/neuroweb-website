$(function() {

	var header, logoWrapper, menuWrapper, innerContent;
	var scrollingBreakpoint = 60;
	var scrollingHeaderDisplayed;

	header = $('.header-page-wrapper');
	logoWrapper = header.find('.logo-wrapper');
	menuWrapper = header.find('.menu-wrapper');
	innerContent = $('.inner-content');
	$(window).load(function() {
		scrollingBreakpoint = logoWrapper.outerHeight();
		decideHeaderState();

	});

	var transitionDuration = 5000;
	function displayScrollingHeader() {
		header.addClass('scrolling');
		innerContent.css('padding-top', menuWrapper.outerHeight());
		scrollingHeaderDisplayed = true;
	}

	function displayNormalHeader() {
		header.removeClass('scrolling');
		innerContent.css('padding-top', 0);
		scrollingHeaderDisplayed = false;
	}

	function decideHeaderState() {
		var scrollTop;
		scrollTop = $(document).scrollTop();
		if (scrollTop >= scrollingBreakpoint && !scrollingHeaderDisplayed) {
			displayScrollingHeader();
		} else if (scrollTop < scrollingBreakpoint && scrollingHeaderDisplayed)  {
			displayNormalHeader();
		}
	}

	$(document).scroll(function() {
		decideHeaderState();
	});

});