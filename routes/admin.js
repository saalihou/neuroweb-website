module.exports = function (app, config, globals) {
	// Required modules
	var passport = require('passport'),
		nodemailer = require('nodemailer'),
		random = require('node-random'),
		checklogin = require(config.middlewares.checklogin)(app, config),
		db = require(config.helpers.dbgetters)(app, config, globals);

	var User = db.getUsersDb();

	// app.get('/admin', function(req, res) {
	// 	res.render('admin/index');
	// });

	// app.get('/admin/content', function(req, res) {
	// 	res.render('admin/content-editing');
	// });

	// app.get('/admin/login', function (req, res) {
	// 	res.render('admin_login');
	// });

	// app.post('/admin/login', passport.authenticate('login', {
	// 	successRedirect: '/',
	// 	failureRedirect: '/admin/login',
	// }));

	// app.get('/admin/subscribe', checklogin, function (req, res) {
	// 	res.render('admin_subscribe');
	// });

	var transporter = nodemailer.createTransport("SMTP", {
		service: 'gmail',
		auth: {
			user: '***@gmail.com',
			pass: '***'
		}
	});

	var mailOptions = {
		from: '***@gmail.com',
		subject: 'Mot de passe NeuroWeb'
	}
	// TODO: middleware that checks the presence of all fields in the request body
	// app.post('/admin/subscribe', checklogin, function (req, res) {
	// 	var user;
	// 	var randompassword;

	// 	random.strings({}, function (err, randomstring) {
	// 		if (err) {
	// 			// TODO: redirect to custom error page
	// 			return res.sendStatus(500);
	// 		}

	// 		randompassword = randomstring;
	// 		mailOptions.to = req.body.email;
	// 		mailOptions.text = 'Your password: ' + randompassword;

	// 		user = {
	// 			username: req.body.username,
	// 			password: randompassword,
	// 			firstName: req.body.firstName,
	// 			lastName: req.body.lastName,
	// 		}

	// 		User.insert(user, function (err, user) {
	// 			if (err) {
	// 				// TODO: redirect to custom error page
	// 				return res.sendStatus(500);
	// 			}

	// 			res.render('admin_subscribe');
	// 		});

	// 		transporter.sendMail(mailOptions, function (err, info) {
	// 			if (err) {
	// 				return console.log(err);
	// 			}

	// 			console.log(info);
	// 		});
	// 	});
	// });
};