module.exports = function (app, config, globals) {

	var feedpagecontent = require(config.middlewares.feedpagecontent)(app, config, globals);

	app.get('/portfolio', feedpagecontent, function (req, res) {
		res.params = res.params || {};
		res.params.path = req.path;

		res.render('portfolio/index', res.params);
	});
};