module.exports = function (app, config, globals) {

	var dbGetters = require(config.helpers.dbgetters)(app, config, globals);

	var Prestation = dbGetters.getPrestationsDb();

	var feedpagecontent = require(config.middlewares.feedpagecontent)(app, config, globals);

	var prestations;
	Prestation.find({}).sort({order: 1}).exec(function(err, prestationsDocs) {
		if (err) {
			// TODO custom error page
			res.sendStatus(500);
		}

		prestations = prestationsDocs;
	});

	app.get('/prestations', feedpagecontent, function (req, res) {
		res.params = res.params || {};
		res.params.path = req.path;

		res.params.prestations = prestations;

		res.render('prestations/index', res.params);
	});
};